import React, { Component } from 'react';
import "normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
// import { MultiSelect } from "@blueprintjs/select";
// import { MultiSelectExample } from './multiSelectExample';
import './App.css';
import CustomMultiSelect from './CustomMultiSelect';

class App extends Component {

  render() {
    return (
      <div className="container">        
        <h1>Select the Countries : </h1> 
          <CustomMultiSelect height="50px" width="900px" bgColor="red" />           
      </div>
    );
  }
}

export default App;