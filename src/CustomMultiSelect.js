import React, { Component } from 'react';
import { Button, Intent, MenuItem } from "@blueprintjs/core";
import { MultiSelect } from "@blueprintjs/select";
import {    
    arrayContainsData, 
    areDatasEqual,  
    dataSelectProps,
    maybeAddCreatedDataToArrays,
    maybeDeleteCreatedDataFromArrays,
    INPUT_DATA,
} from "./multiselectController";
import styled from 'styled-components'

const MultiSelectStyle = styled(MultiSelect)`
  width: ${props => props.width || '300px' } !important;
  height: ${props => props.height || '300px' };
  display: inline-block;  
  
  .bp3-tag-input{
    background: ${props => props.bgColor || 'grey' } !important;
  }
  .bp3-input-ghost{
    width: ${props => props.width || '300px' } !important;
  }
`;

const INTENTS = [Intent.NONE, Intent.PRIMARY, Intent.SUCCESS, Intent.DANGER, Intent.WARNING];

class CustomMultiSelect extends Component {
    constructor(props){
        super(props);
        this.state = {
            allowCreate: false,
            createdItems: [],
            fill: false,
            datas: [],
            hasInitialContent: false,
            intent: false,
            items: dataSelectProps.items,
            openOnKeyDown: false,
            popoverMinimal: true,
            resetOnSelect: true,
            tagMinimal: false,
        };         
    }

    selection = () => {
        return this.state.datas;
    }

    render() {

        const { allowCreate, datas, hasInitialContent, tagMinimal, popoverMinimal, ...flags } = this.state;

        const getTagProps = (_value, index) => ({
            intent: this.state.intent ? INTENTS[index % INTENTS.length] : Intent.NONE,
            minimal: tagMinimal,
        });

        const initialContent = this.state.hasInitialContent ? (
            <MenuItem disabled={true} text={`${INPUT_DATA.length} items loaded.`} />
        ) : // explicit undefined (not null) for default behavior (show full list)
        undefined;

        const clearButton =
        datas.length > 0 ? <Button icon="cross" minimal={true} onClick={this.handleClear} /> : undefined;


        return (           
                <MultiSelectStyle
                        {...dataSelectProps}
                        {...flags}                   
                        initialContent={initialContent}
                        itemRenderer={this.renderData}
                        itemsEqual={areDatasEqual}
                        // we may customize the default filmSelectProps.items by
                        // adding newly created items to the list, so pass our own
                        items={this.state.items}
                        noResults={<MenuItem disabled={true} text="No results." />}
                        onItemSelect={this.handleDataSelect}
                        onItemsPaste={this.handleDatasPaste}
                        popoverProps={{ minimal: popoverMinimal }}
                        tagRenderer={this.renderTag}                   
                        tagInputProps={{
                            onRemove: this.handleTagRemove,
                            rightElement: clearButton,                        
                            tagProps: getTagProps,
                        }}
                        selectedItems={this.state.datas}
                        height={this.props.height}
                        width={this.props.width}
                        bgColor={this.props.bgColor}
                    /> 
        );
    }

    renderTag = (data) => data.id + '. ' + data.value ;

    renderData = (data, { modifiers, handleClick }) => {
        if (!modifiers.matchesPredicate) {
            return null;
        }
        return (
            <MenuItem
                active={modifiers.active}
                icon={this.isDataSelected(data) ? "tick" : "blank"}
                key={data.id}               
                onClick={handleClick}
                text={`${data.id}. ${data.value}`}
                shouldDismissPopover={false}
            />
        );
    };

    handleTagRemove = (_tag, index) => {
        this.deselectData(index);
    };


    isDataSelected(data) {
        return this.getSelectedDataIndex(data) !== -1;
    }

    getSelectedDataIndex(data) {
        return this.state.datas.indexOf(data);
    }
    
    handleDataSelect = (data) => {
        if (!this.isDataSelected(data)) {
            this.selectData(data);
        } else {
            this.deselectData(this.getSelectedDataIndex(data));
        }
    };

    selectData(data) {
        this.selectDatas([data]);
    }

    selectDatas(datasToSelect) {
        const { createdItems, datas, items } = this.state;

        let nextCreatedItems = createdItems.slice();
        let nextDatas = datas.slice();
        let nextItems = items.slice();

        datasToSelect.forEach(data => {
            const results = maybeAddCreatedDataToArrays(nextItems, nextCreatedItems, data);
            nextItems = results.items;
            nextCreatedItems = results.createdItems;
            // Avoid re-creating an item that is already selected (the "Create
            // Item" option will be shown even if it matches an already selected
            // item).
            nextDatas = !arrayContainsData(nextDatas, data) ? [...nextDatas, data] : nextDatas;
        });

        this.setState({
            createdItems: nextCreatedItems,
            datas: nextDatas,
            items: nextItems,
        });

        // console.log(this.state);
    }

    deselectData(index) {
        const { datas } = this.state;

        const data = datas[index];
        const { createdItems: nextCreatedItems, items: nextItems } = maybeDeleteCreatedDataFromArrays(
            this.state.items,
            this.state.createdItems,
            data,
        );

        // Delete the item if the user manually created it.
        this.setState({
            createdItems: nextCreatedItems,
            datas: datas.filter((_data, i) => i !== index),
            items: nextItems,
        });
    }

    handleDatasPaste = (datas) => {
        // On paste, don't bother with deselecting already selected values, just
        // add the new ones.
        this.selectDatas(datas);
    };

    handleClear = () => this.setState({ datas: [] });
}

export default CustomMultiSelect;